/*
 
 LCDI2C v0.4 3/Mar/2009 dale@wentztech.com  http://wentztech.com/Radio
 PORTED FOR Matrix Orbital LK162-12 display 10/Sept/2010 by Brian Brumfield, brian@thebrumfields.com
 
 What is this?
 
 An arduino library for use with the web4robot.com i2C LCD Display in I2C more
 
 Uses I2c Wires interface
 
 Uses Analog pin 4 - SDA
 Uses Analog pin 5 - SCL
 
 
 Usage:
 
 see the examples folder of this library distribution.
 
 
 */


#include <Wire.h>


#include <string.h> //needed for strlen()

#include <inttypes.h>

#include "Arduino.h"  //all things wiring / arduino

#include "LCDI2C_LK162-12.h"


#define LCDI2C_MAX_STRLEN			40
#define LCDI2C_PRINT_STR_DELAY		20


//--------------------------------------------------------

// (don't change here - specify on calling constructor)

//how many lines has the LCD? 

int g_num_lines = 2;

int g_num_col = 16;

// Defalt address of the display

int g_i2caddress = 0x50;

int g_display = 0;


//stuff the library user might call---------------------------------

//constructor.  num_lines must be 1, 2, 3, or 4 currently.

LCDI2C::LCDI2C (int num_lines,int num_col,int i2c_address,int display){
	
	g_num_lines = num_lines;
	g_num_col = num_col;
	g_i2caddress = i2c_address >>1; // ** 7 bit address
	g_display = display;
	
	if (g_num_lines < 1 || g_num_lines > 4){
		
		g_num_lines = 2;
		
	}
	
	if (g_num_col < 1 || g_num_col > 40){
		
		g_num_col = 16;
	}
	
}

//	Send a command to the display that is not supported

void LCDI2C::command(int value) {
	
	Wire.beginTransmission(g_i2caddress);
	Wire.write(0xFE);
	Wire.write(value);
	Wire.endTransmission();
	delay(CMDDELAY);
}

// Write Start-Up Screen

void LCDI2C::writeStartUpScreen(char message[32]) {
	
	Wire.beginTransmission(g_i2caddress);
	Wire.write(254); //0xFE);
	Wire.write(64); //0x40);
	Wire.write(message);
	Wire.endTransmission();
	delay(CMDDELAY);
}


//Used by the print library to get information to the display

size_t LCDI2C::write(uint8_t value) {
	
	Wire.beginTransmission(g_i2caddress);
	Wire.write(value);
	Wire.endTransmission();
	delay(5);
        return 0;	
}




//send the clear screen command to the LCD

void LCDI2C::clear(){
	
	Wire.beginTransmission(g_i2caddress);
	Wire.write(0xFE);
	Wire.write(0x58);
	Wire.endTransmission();
	delay(CMDDELAY);  
	
	
}

//send the Home Cursor command to the LCD      ********** Not Working ***************

void LCDI2C::home(){
	
	setCursor(0,0);					// The command to home the cursor does not work on the version of the dislay I have
	// So we do it this way.
}


//Turn the LCD ON

void LCDI2C::on(int value){
	
	Wire.beginTransmission(g_i2caddress);
	Wire.write(0xFE);
	Wire.write(0x42);
	Wire.write(value);
	Wire.endTransmission();
	delay(CMDDELAY);  
}


// Turn the LCD OFF

void LCDI2C::off(){
	
	Wire.beginTransmission(g_i2caddress);
	Wire.write(0xFE);
	Wire.write(0x46);
	Wire.endTransmission();
	delay(CMDDELAY);
	
}


//Turn the Underline Cursor ON

void LCDI2C::cursor_on(){
	
	Wire.beginTransmission(g_i2caddress);
	Wire.write(0xFE);
	Wire.write(0x4A);
	Wire.endTransmission();
	delay(CMDDELAY);
	
}


//Turn the Underline  Cursor OFF

void LCDI2C::cursor_off(){
	
	Wire.beginTransmission(g_i2caddress);
	Wire.write(0xFE);
	Wire.write(0x4B);
	Wire.endTransmission();
	delay(CMDDELAY);
	
}


//Turn the Underline Cursor ON

void LCDI2C::blink_on(){
	
	Wire.beginTransmission(g_i2caddress);
	Wire.write(0xFE);
	Wire.write(0x53);
	Wire.endTransmission();
	delay(CMDDELAY);
	
}


//Turn the Underline  Cursor OFF

void LCDI2C::blink_off(){
	
	Wire.beginTransmission(g_i2caddress);
	Wire.write(0xFE);
	Wire.write(0x54);
	Wire.endTransmission();
	delay(CMDDELAY);
	
}


//Move the cursor left 1 space

void LCDI2C::left(){
	
	Wire.beginTransmission(g_i2caddress);
	Wire.write(0xFE);
	Wire.write(0x43);
	Wire.endTransmission();
	delay(CMDDELAY);
	
}


//Move the cursor right 1 space

void LCDI2C::right(){
	
	Wire.beginTransmission(g_i2caddress);
	Wire.write(0xFE);
	Wire.write(0x4D);
	Wire.endTransmission();
	delay(CMDDELAY);
	
}


// initiatize lcd after a short pause

//while there are hard-coded details here of lines, cursor and blink settings, you can override these original settings after calling .init()

void LCDI2C::init () {
	delay(1000);
	Wire.begin();
	on(5);
	clear();
	blink_off();
	cursor_off(); 
	home();
}

void LCDI2C::setCursor(int line_num, int x){
	
	Wire.beginTransmission(g_i2caddress);
	Wire.write(0xFE);
	Wire.write(0x47);
	Wire.write(line_num);
	Wire.write(x);
	Wire.endTransmission();
	delay(POSDELAY);
	
}

int LCDI2C::keypad (){
	
	int data = 0;
	
	//  Send Keypad read command
	Wire.beginTransmission(g_i2caddress);
	Wire.write(0xFE);
	Wire.write(0x26);
	Wire.endTransmission();
	delay(CMDDELAY);
	
	//  Connect to device and request byte
	Wire.beginTransmission(g_i2caddress);
	Wire.requestFrom(g_i2caddress, 1);
	
	if (Wire.available()) {
		data = Wire.read();
	}
	
	
	return data;
}

unsigned char LCDI2C::init_bargraph(unsigned char graphtype)
{
	switch (graphtype)
	{
		case LCDI2C_THICK_VERTICAL_BAR_GRAPH:
			Wire.beginTransmission(g_i2caddress);
			Wire.write(0xFE);
			Wire.write(0x76);
			Wire.endTransmission();
			break;
		case LCDI2C_THIN_VERTICAL_BAR_GRAPH:
			Wire.beginTransmission(g_i2caddress);
			Wire.write(0xFE);
			Wire.write(0x73);
			Wire.endTransmission();
			break;
		case LCDI2C_HORIZONTAL_BAR_GRAPH:
			Wire.beginTransmission(g_i2caddress);
			Wire.write(0xFE);
			Wire.write(0x68);
			Wire.endTransmission();
			break;
		case LCDI2C_HORIZONTAL_LINE_GRAPH:
			Wire.beginTransmission(g_i2caddress);
			Wire.write(0xFE);
			Wire.write(0x16);
			Wire.write(0x01);
			Wire.endTransmission();
			break;
		default:
			return 1;
	}
	
	return 0;
}

void LCDI2C::draw_horizontal_graph(unsigned char row, unsigned char column, unsigned char len,  unsigned char pixel_col_end)
{
	Wire.beginTransmission(g_i2caddress);
	Wire.write(0xFE);
	Wire.write(0x7C);
	Wire.write(row);
	Wire.write(column);
	Wire.write(len);
	Wire.write(pixel_col_end);
	Wire.endTransmission();
}

void LCDI2C::draw_vertical_graph(unsigned char row, unsigned char column, unsigned char len,  unsigned char pixel_row_end)
{
	Wire.beginTransmission(g_i2caddress);
	Wire.write(0xFE);
	Wire.write(0x3D);
	Wire.write(row);
	Wire.write(column);
	Wire.write(len);
	Wire.write(pixel_row_end);
	Wire.endTransmission();
}

void LCDI2C::load_custom_character(unsigned char char_num, unsigned char *rows)
{
	unsigned char i;
	
	Wire.beginTransmission(g_i2caddress);
	Wire.write(0xFE);
	Wire.write(0x4E);
	Wire.write(char_num);
	for (i = 0; i < LCDI2C_CUSTOM_CHAR_SIZE; i++)
		Wire.write(rows[i]);
	Wire.endTransmission();
}

unsigned char LCDI2C::set_backlight_brightness(unsigned char new_val)
{
	if ((new_val < LCDI2C_MIN_BRIGHTNESS)
		|| (new_val > LCDI2C_MAX_BRIGHTNESS))
		return LCDI2C_VALUE_OUT_OF_RANGE;
	
	Wire.beginTransmission(g_i2caddress);
	Wire.write(0xFE);
	Wire.write(0x99);
	Wire.write(new_val);
	Wire.endTransmission();
	return 0;
}


unsigned char LCDI2C::set_contrast(unsigned char new_val)
{
	if ((new_val < LCDI2C_MIN_CONTRAST)
		|| (new_val > LCDI2C_MAX_CONTRAST))
		return LCDI2C_VALUE_OUT_OF_RANGE;
	
	Wire.beginTransmission(g_i2caddress);
	Wire.write(0xFE);
	Wire.write(0x50);
	Wire.write(new_val);
	Wire.endTransmission();
	return 0;
}

// Overload 
void  LCDI2C::printstr(const char c[])
{
	byte len;
	
	while (*c)
	{
		len = min(strlen(c), LCDI2C_MAX_STRLEN);
		Wire.beginTransmission(g_i2caddress);
		Wire.write(0xFE);
		Wire.write(0x15);
		Wire.write(len);
		while (len--)
			Wire.write(*c++);
		Wire.endTransmission();
		if (*c)
			delay(LCDI2C_PRINT_STR_DELAY);	// More to send.  Wait a bit
	}
}

